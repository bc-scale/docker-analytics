# Docker Analytics

Imagem do [Docker](https://www.docker.com/) contendo ambiente de trabalho reprodutível para as análises do Observatório de Fortaleza do Instituto de Planejamento de Fortaleza (IPLANFOR).

Contém as ferramentas:
- [Apache Spark](https://spark.apache.org/) para o pré-processamento dos dados;
- [Jupyterlab](https://jupyter.org/) com kernels do [Python](https://www.python.org/) e do [R](https://www.r-project.org/) para investigação interativa;
- [Apache Airflow](https://airflow.apache.org/) para desenvolvimento dos *pipelines*.

# Dependências

## Obrigatório

- [Docker](https://www.docker.com/)
- [Compose](https://docs.docker.com/compose/)

## Opcional

- [GNU Make](https://www.gnu.org/software/make/)

# Gerando as imagens

```
docker compose build
```

# Inicializando

Para iniciar a aplicação:
```
docker compose up
```

# Checagem

Verifique se você consegue acessar:
- JupyterLab: http://localhost:8888
- Airflow: http://localhost:8080
- Spark Master: http://localhost:8181

# Encerrando

```
docker compose down
```
