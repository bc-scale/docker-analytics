.PHONY: build

app = diobs

# Constrói as imagens do Docker
build:
	docker compose build

up: build
	docker compose --project-name $(app) up

down: docker-compose.yml
	docker compose --project-name $(app) down

cleanall: docker-compose.yml
	docker compose --project-name $(app) down --volumes --rmi all
